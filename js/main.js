function createNewUser() {

  let newUser = {
    firstName: prompt("What is your name?"),
    lastName: prompt("What is your surname?"),
    getLogin: function () {
      
      return ((this.firstName.charAt(0) + this.lastName).toLowerCase());
    }
  }

  Object.defineProperties(newUser, {
    firstName: { writable: false },
    lastName: { writable: false },
  }),

  console.log(newUser);
  
  return newUser;
}

const user = createNewUser();

console.log(user.getLogin());
